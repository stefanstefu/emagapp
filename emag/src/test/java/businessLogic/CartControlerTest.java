package businessLogic;

import model.Item;
import model.Laptop;
import org.junit.Test;
import shared.Processor;

import java.util.Map;

import static org.junit.Assert.*;

public class CartControlerTest {

    @Test
    public void userAddItemToCart() throws Exception {
        //Given
        Stock stock = new Stock();
        Cart cart = new Cart();
        CartControler cartControler = new CartControler(stock,cart);
        Laptop laptop1 = new Laptop("Lenovo",16, Processor.AMD,1600);
        stock.addSupply(laptop1,100);

        //When
        int itemsAdded = cartControler.userAdd(laptop1,5);
        int itemsWantedToBeAdded = 5;

        //Then
        assertEquals(itemsWantedToBeAdded, itemsAdded);
        System.out.println("Items wanted to be added " + itemsWantedToBeAdded + " and Items added " + itemsAdded);

    }

    @Test
    public void removingOneItemThatYouWant() {
        //Given
        Stock stock = new Stock();
        Cart cart = new Cart();
        CartControler cartControler = new CartControler(stock,cart);
        Laptop laptop1 = new Laptop("Lenovo",16, Processor.AMD,1600);
        stock.addSupply(laptop1,100);
        cart.addCartItem(laptop1,10);

        //When
        int itemsToRemove = cartControler.userRemove(laptop1,5);
        int itemsWantedToBeRemoved = 5;

        //Then
        assertEquals(itemsWantedToBeRemoved, itemsToRemove);
        System.out.println("Items wanted to be removed " + itemsWantedToBeRemoved + " and Items removed " + itemsToRemove);

    }

    @Test
    public void removingAllItemsFromCart() throws Exception {
        //Given
        Stock stock = new Stock();
        Cart cart = new Cart();
        CartControler cartControler = new CartControler(stock,cart);
        Laptop laptop1 = new Laptop("Lenovo",16, Processor.AMD,1600);
        Laptop laptop2 = new Laptop("Lenovo",16, Processor.AMD,1600);
        stock.addSupply(laptop1,100);
        stock.addSupply(laptop2,100);
        cart.addCartItem(laptop1,10);
        cart.addCartItem(laptop2,10);

        //When
        cartControler.useRemoveAll();

        //Then
         assertTrue(cartControler.useRemoveAll().isEmpty());

    }


    @Test
    public void removingInexistingItem(){
        //Given
        Stock stock = new Stock();
        Cart cart = new Cart();
        CartControler cartControler = new CartControler(stock,cart);
        Laptop laptop1 = new Laptop("Lenovo",16, Processor.AMD,1600);
        Laptop laptop2 = new Laptop("Lenovo",16, Processor.AMD,1600);
        stock.addSupply(laptop1,100);
        stock.addSupply(laptop2,100);
//        cart.addCartItem(laptop1,10);
//        cart.addCartItem(laptop2,10);


        //When

        cartControler.userRemove(laptop1,10);

        //Then

        assertEquals(0, cartControler.userRemove(laptop1,10));

    }

}