package businessLogic;

import model.Item;

import java.util.List;
import java.util.Map;

public class StockControler {

    private final Stock stock;


    public StockControler(Stock stock) {
        this.stock = stock;
    }

    public List<Map.Entry<Item, Integer>> search(String itemName){
        return stock.search(itemName);
    }

}
