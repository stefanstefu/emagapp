package businessLogic;

import model.Item;

import java.util.Map;

public class CartControler {

    private final Stock stock;
    private final Cart cart;

    public CartControler(Stock stock, Cart cart) {
        this.stock = stock;
        this.cart = cart;
    }

    public int userAdd(Item item, int quantity) throws Exception {
        final int availableStrock = stock.consumeSupply(item, quantity);
        final int addedInCart = cart.addCartItem(item, availableStrock);
        return addedInCart;
    }

    public int userRemove(Item item, int quantity) {
        final int removeFromCart = cart.removeItem(item, quantity);
        final int addToStock = stock.addSupply(item, removeFromCart);
        return addToStock;
    }


    public Map<Item, Integer> useRemoveAll() throws Exception {

        final Map<Item, Integer> removedItems = cart.removeAll();
        for (Map.Entry<Item, Integer> entry : removedItems.entrySet()) {
            stock.consumeSupply(entry.getKey(), entry.getValue());
        }
        return removedItems;
    }

}
