package businessLogic;

import model.Item;

import java.util.List;
import java.util.Map;

public class Client implements Runnable {
    private final CartControler cartControler;
    private final Item itemToBuy;
    private final StockControler stockControler;



    public Client(Stock stock, Item itemToBuy, StockControler stockControler) {
        this.cartControler = new CartControler(stock, new Cart());
        this.itemToBuy = itemToBuy;
        this.stockControler = stockControler;
    }

    public int userAdd(Item item, int quantity) {
        try {
            return cartControler.userAdd(item, quantity);
        } catch (Exception e) {
//            System.out.println(e.getMessage());
        }
        return 0;
    }

    public int userRemove(Item item, int quantity) {
        return cartControler.userRemove(item, quantity);
    }

    public void useRemoveAll() throws Exception {
        cartControler.useRemoveAll();
    }

    public List<Map.Entry<Item, Integer>> search(String itemName){
        return stockControler.search(itemName);
    }

    @Override
    public void run() {
        int requestCount = 0;
        int addetItems =0;
        while (requestCount++ <= 100000) {
            addetItems+= userAdd(itemToBuy,1);
        }
        System.out.println("Added items: " + addetItems);

    }
}
