package businessLogic;

import model.Item;
import model.Laptop;
import model.SearchResult;

import java.io.*;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Stock implements Serializable {

    private Map<Item, Integer> stockSuply = new ConcurrentHashMap<>();
    private static final int MAX_QUANTITY = 100000;
    private final static long STOCK_DB_FILENAME = 1L;

    public Map<Item, Integer> getStockSuply() {
        return stockSuply;
    }

    public synchronized int addSupply(Item item, int quantity) {
        if (!stockSuply.containsKey(item)) {
            if (quantity > MAX_QUANTITY) {
                stockSuply.put(item, MAX_QUANTITY);
                return MAX_QUANTITY;
            }
            stockSuply.put(item, quantity);
            return quantity;
        }
        final int existingQuantity = stockSuply.get(item);
        int updatedQuantity = existingQuantity + quantity;
        if (updatedQuantity > MAX_QUANTITY) {
            updatedQuantity = MAX_QUANTITY;
        }
        stockSuply.put(item, updatedQuantity);
        return updatedQuantity - existingQuantity;
    }

    public synchronized int consumeSupply(Item item, int quantity) throws Exception {
        if (!stockSuply.containsKey(item) || stockSuply.get(item) == 0) {
            throw new Exception(item.getName() + "is out of stock");

        }

        final int existingQuantity = stockSuply.get(item);
        int updatedQuantity = existingQuantity - quantity;
        if (updatedQuantity < 0) {
            stockSuply.put(item, 0);
            return existingQuantity;
        }
        stockSuply.put(item, updatedQuantity);
        return quantity;
    }


    /**
     * The bellow method is more hard coded,
     * we used Map.entry because this is how you acces the map
     * We didn't use optional, or function , or anything
     */
    public List<Map.Entry<Item, Integer>> search(String itemName) {

        return stockSuply.entrySet().stream().
                filter(entry -> entry.getKey().getName().equals(itemName))
                .collect(Collectors.toList());
    }

    /**
     * The bellow method is the elegant way of deffining the method by using another class
     * SearchResult.
     * And Predicate
     */
//    public List<SearchResult> searchWithElegance(String itemName) {
//        Predicate<SearchResult<Laptop>> fileConditions = searchResult ->
//                searchResult.getItem().getName().toLowerCase()
//                        .matches(".*" + itemName.toLowerCase() + ".*");
//
//        return stockSuply.entrySet().stream()
//                .map(entry -> new SearchResult(entry.getKey(), entry.getValue()))
//                .filter((Predicate<? super SearchResult>) fileConditions).collect(Collectors.toList());
//    }

    /**
     * Price filter method
     * TO DO : Terminate the LOW PRICE filter Method
     */

    public List<String> searchByPrice(int price) {
        List<String> lowPriceItems = stockSuply.entrySet().stream().filter(entry -> entry.getValue().equals(price))
                .map(entry -> entry.getKey().getName())
                .collect(Collectors.toList());
        return lowPriceItems;
    }

    public void stockSuplySaving() throws IOException {

        BufferedWriter writer = new BufferedWriter(new FileWriter("stockSuply"));
        writer.write(getStockSuply().toString());
        writer.flush();
        writer.close();
    }


    public void saveStockState() throws IOException {
        ObjectOutputStream fileSave = new ObjectOutputStream(new FileOutputStream("stockFileToSave.db"));
        fileSave.writeObject(this);
        fileSave.close();
    }
    public void loadStockState() throws IOException, ClassNotFoundException {
        ObjectInputStream fileSave = new ObjectInputStream(new FileInputStream("stockFileToSave.db"));
        Stock loadedStock = (Stock) fileSave.readObject();
        stockSuply.clear();
        stockSuply.putAll(loadedStock.stockSuply);
        fileSave.close();
    }


    @Override
    public String toString() {
        return "Stock{" +
                "stockSuply=" + stockSuply +
                '}';
    }
}
