package businessLogic;

import model.Item;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.stream.Collectors;


public class Cart {
    private static final int ADDITION_SIGN = 1;
    private static final int SUBTRACT_SIGN = -1;
    private static final String COLUMN_SEPARATOR = " ";
    private static final String CHECKOUT_TAG = "Checkout cart";
    private static final String TOTAL_TAG = "Total: ";

    private Map<Item, Integer> shoppingCart = new ConcurrentHashMap<>();
    private int total;

    /**
     * Adds item to cart.
     *
     * @param item     item to be added to the cart.
     * @param quantity quantity of the item to be added to the cart.
     * @return the added quantity to the cart.
     */
    public int addCartItem(Item item, int quantity) {
        updateTotal(item, quantity, ADDITION_SIGN);
        if (!shoppingCart.containsKey(item)) {
            shoppingCart.put(item, quantity);
            return quantity;
        }

        int existingQuantity = shoppingCart.get(item);
        int updatedQuantity = existingQuantity + quantity;
        shoppingCart.put(item, updatedQuantity);
        return quantity;
    }

    private void updateTotal(Item item, int quantity, int sign) {
        total += quantity * item.getPrice() * sign;
    }

    public int removeItem(Item item, int quantity) {
        if (!shoppingCart.containsKey(item)) {
            System.err.println("Item not in the cart");
            System.out.println(shoppingCart);
            return 0;
        }

        int existingQuantity = shoppingCart.get(item);
        int updatedQuantity = existingQuantity - quantity;
        if (updatedQuantity > 0) {
            shoppingCart.put(item, updatedQuantity);
            updateTotal(item, quantity, SUBTRACT_SIGN);
            return quantity;
        }
        shoppingCart.put(item, 0);
        updateTotal(item, existingQuantity, SUBTRACT_SIGN);
        return existingQuantity;
    }

    // in ortder for the cartControler remove all method to work we`ve made a copy of hashmap;

    public Map<Item, Integer> removeAll() {
       final Map<Item, Integer> snapshotCart = new ConcurrentHashMap<>(shoppingCart);
        shoppingCart = new HashMap<>();     // Garbage collector in action
        // shoppingCart.clear();            // Reuse existing map
        total = 0;
        return snapshotCart;
    }

    public String checkout() {
        final StringBuilder checkout = getCheckoutDetails();
        removeAll();
        return checkout.toString();
    }

    private StringBuilder getCheckoutDetailsWithStreams() {
        final StringBuilder checkout = getCheckoutHeader();
        Predicate<Map.Entry<Item,Integer>> predicate = (item) -> item.getValue()!= 0;

        final String lineDetails = shoppingCart.entrySet().stream()
                .filter(predicate)
                .map(this::getCheckOutLineDetails)
                .collect(Collectors.joining(System.lineSeparator()));
        checkout.append(lineDetails).append(TOTAL_TAG).append(total).
                append(System.lineSeparator());

        return checkout;

    }

    private StringBuilder getCheckoutDetails() {
        final StringBuilder checkout = getCheckoutHeader();
        for (final Map.Entry<Item, Integer> entry : shoppingCart.entrySet()) {
            final StringBuilder currentLine = getCheckOutLineDetails(entry);
            checkout.append(currentLine).append(System.lineSeparator());
            // Predicate<Item> predicate = (item) -> item.getPrice() > 0;
        }
        checkout.append(TOTAL_TAG).append(total).append(System.lineSeparator());
        return checkout;
    }

    private StringBuilder getCheckOutLineDetails(Map.Entry<Item, Integer> entry) {
        final StringBuilder currentLine = new StringBuilder();

        final Item currentItem = entry.getKey();
        final int price = currentItem.getPrice();
        currentLine.append(currentItem.getName()).append(COLUMN_SEPARATOR);
        currentLine.append(price).append(COLUMN_SEPARATOR);

        final int currentQuantity = entry.getValue();
        currentLine.append(currentQuantity).append(COLUMN_SEPARATOR);
        currentLine.append(currentQuantity * price).append(COLUMN_SEPARATOR);
        return currentLine;
    }

    private StringBuilder getCheckoutHeader() {
        return new StringBuilder(CHECKOUT_TAG)
                .append(System.lineSeparator());
    }

    /**
     *One method of writing to a file the output of a method
     * with BufferedWriter
     * checkout;
     */

    public void writeCheckoutData() throws IOException {
        BufferedWriter writer  = new BufferedWriter(new FileWriter("fileToWrite"));
        writer.write(checkout());
        writer.flush();
        writer.close();
    }

    /**
     * Writting a file with new IO
     * @throws IOException
     */
    public void wirteFileWithNio() throws IOException {
        String checkout = checkout();
        Path path = Paths.get("print_checkout.txt");
        final BufferedWriter writer = Files.newBufferedWriter(path);
        writer.write(checkout);
        writer.flush();
        writer.close();
    }



}
