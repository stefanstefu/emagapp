package model;

public class SearchResult<T> {

    private T item;
    private T quantity;

    public SearchResult(T item, T quantity) {
        this.item = item;
        this.quantity = quantity;
    }

    public T getItem() {
        return item;
    }

    public T getQuantity() {
        return quantity;
    }
}
