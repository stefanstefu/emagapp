package model;

import shared.Processor;

public class Laptop extends Item {

    private int memorie;
    private Processor procesor;

    public int getMemorie() {
        return memorie;
    }

    public Processor getProcesor() {
        return procesor;
    }

    public Laptop(String name, Integer memorie, Processor procesor,Integer price) {
        super(name);
        this.memorie = memorie;
        this.procesor = procesor;
        this.price = price;
    }

    @Override
    public String toString() {
        return "\nLaptop=" + getName()+
                " ;memorie=" + memorie + "GB" +
                ", procesor=" + procesor +
                ", price=" + price +"euro ;" +
                "Pieces";
    }
}
