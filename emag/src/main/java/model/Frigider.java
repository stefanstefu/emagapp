package model;

import shared.Consum;

public class Frigider extends Item {

    private Consum consum;
    private Integer volum;
    ;

    public Frigider(String name, Consum consum, Integer volum, Integer price) {
        super(name);
        this.consum = consum;
        this.volum = volum;
        this.price = price;

    }

    public Consum getConsum() {
        return consum;
    }

    public int getVolum() {
        return volum;
    }


    @Override
    public String toString() {
        return "Frigider[" +
                "consum=" + consum +
                ", volum=" + volum +
                ", price=" + price +
                ']';
    }
}
