package compile;

import businessLogic.*;
import model.Item;
import model.Laptop;
import shared.Processor;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws Exception {
        final Stock stock = new Stock();
        final StockControler stockControler = new StockControler(stock);
        final Item laptopLenovo = new Laptop("Lenovo", 16, Processor.AMD, 1600);
        final Item laptopLenovo2 = new Laptop("dell", 16, Processor.AMD, 5000);
        final Item laptopLenovo3 = new Laptop("intel", 16, Processor.AMD, 1200);
        final Item laptopLenovo4 = new Laptop("Lenovo23", 16, Processor.AMD, 1600);
        final Item laptopLenovo5 = new Laptop("Lenovo11", 16, Processor.AMD, 1800);
        final Item laptopDell = new Laptop("Dell", 16, Processor.AMD, 1600 );
        Cart cart1 = new Cart();
        Client client = new Client(stock,laptopLenovo,stockControler);
        CartControler cartControler = new CartControler(stock,cart1);
        stock.addSupply(laptopLenovo, 100000);
        stock.addSupply(laptopLenovo2, 10000);
        stock.addSupply(laptopLenovo3, 10000);
        stock.addSupply(laptopLenovo4, 10000);
        stock.addSupply(laptopDell, 10000);
        final Thread client1 = new Thread(new Client(stock, laptopLenovo, stockControler));
        final Thread client2 = new Thread(new Client(stock, laptopLenovo, stockControler));

        client1.start();
        client2.start();

//        System.out.println(stock.getStockSuply());
//        try {
//            stock.consumeSupply(laptop, 10);
//
//        } catch (Exception e) {
//            System.err.println("Out of Stock, Add items");
//            stock.addSupply(laptop,10);
////            System.out.println(stock.getStockSuply());
//        }
//        stock.getStockSuply();
//        cart.addCartItem(laptop,10);
//        cart.addCartItem(laptop2,30);
//        System.out.println(cart.checkout());


        stock.addSupply(laptopLenovo5,20);
        cart1.addCartItem(laptopLenovo,100);
        cart1.addCartItem(laptopDell,200);
        cart1.writeCheckoutData();
        stock.stockSuplySaving();
//        System.out.println(client.search("Lenovo"));
        stock.saveStockState();
        System.out.println(cartControler.useRemoveAll());
    }
}
